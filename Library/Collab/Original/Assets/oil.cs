﻿using System.Collections;
using UnityEngine;
//random repush comment note, try II
public class oil : MonoBehaviour
{
    void Start()
    {
        var car1 = GameObject.Find("Car1");
        Debug.Log("Car 1 Drift factor seems to be: " + car1.GetComponent<CarPhysics>().DriftFactorSticky);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var collidedCar = collision.gameObject;
        StartCoroutine(StickyCar(collidedCar));
    }

    IEnumerator StickyCar(GameObject car)
    {
        var collidedCarPhysics = car.GetComponent<CarPhysics>();
        var originalFactor = collidedCarPhysics.DriftFactorSticky;
        collidedCarPhysics.DriftFactorSticky = 1.2f;
        yield return new WaitForSeconds(2);
        collidedCarPhysics.DriftFactorSticky = originalFactor;
    }
}
