﻿using UnityEngine;

public class CarPhysics : MonoBehaviour
{
    //outsideControls
    public float steer;
    public bool accel;
    public bool brake;

    //Car physics
    float power = 12f;
    float turnRadius = 100f;

    float driftFactorSlippy = 1f;
    float maxStickyVelocity = 2.5f;

    private float driftFactorSticky = 0.8f; //will be reset automatically in C# v. < 6
    public float DriftFactorSticky { get; set; }

    private void Start()
    {
        this.DriftFactorSticky = driftFactorSticky; //this should do the init sufficiently…
    }

    void FixedUpdate()
    {

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        float driftFactor = driftFactorSticky;
        if (RightVelocity().magnitude > maxStickyVelocity)
        {
            driftFactor = driftFactorSlippy;
        }

        rb.velocity = ForwardVelocity() + RightVelocity() * driftFactor;

        if (accel)
        {
            rb.AddForce(transform.up * power);
        }
        if (brake)
        {
            rb.AddForce(transform.up * -power);
        }

        float tf = Mathf.Lerp(0, -turnRadius, rb.velocity.magnitude / 2);



        rb.angularVelocity = steer * tf;
        Debug.Log(driftFactor);

    }
    Vector2 ForwardVelocity()
    {
        return transform.up * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.up);

    }
    Vector2 RightVelocity()
    {
        return transform.right * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.right);

    }
}
