﻿using System.Collections;
using UnityEngine;
//random repush comment note, try II
public class oil : MonoBehaviour
{
    void Start()
    {
        var car1 = GameObject.Find("Car1");
        Debug.Log("Car 1 Max Stivky Velocity seems to be: " + car1.GetComponent<CarPhysics>().MaxStickyVelocity);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var collidedCar = collision.gameObject;
        StartCoroutine(StickyCar(collidedCar));
    }

    IEnumerator StickyCar(GameObject car)
    {
        var collidedCarPhysics = car.GetComponent<CarPhysics>();            
        var originalGripVelocity = collidedCarPhysics.MaxStickyVelocity;
        collidedCarPhysics.MaxStickyVelocity = 1.9f;        
        Debug.Log("Oil collision of " + car.name + "! Setting MSV to: " + collidedCarPhysics.MaxStickyVelocity.ToString());
        yield return new WaitForSeconds(1);
        collidedCarPhysics.MaxStickyVelocity = originalGripVelocity;
        Debug.Log("Oil collision of " + car.name + "¨ceassed. Resetting MSV to: " + collidedCarPhysics.MaxStickyVelocity);
    }
}
