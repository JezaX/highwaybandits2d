﻿using UnityEngine;

public class CarPhysics : MonoBehaviour
{
    //outsideControls
    public float steer;
    public bool accel;
    public bool brake;

    //Car physics
    float power = 12f;
    float turnRadius = -100f;

    float driftFactorSlippy;
    float driftFactorSticky;
    float maxStickyVelocity;

    public float DriftFactorSticky { get { return driftFactorSticky; } set { this.driftFactorSticky = value; } }
    public float DriftFactorSlippy { get { return driftFactorSlippy; } set { this.driftFactorSlippy = value; } }
    public float MaxStickyVelocity { get { return maxStickyVelocity; } set { this.maxStickyVelocity = value; } }

    private void Start()
    {
        //init values
        this.driftFactorSticky = 0.80f; //kolik driftu kdyz drzi
        this.driftFactorSlippy = 1.4f; //kolik driftu kdyz nedrzi
        this.maxStickyVelocity = 2.5f; //max rychlost pro dobrou trakci
    }

    void FixedUpdate()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        float driftFactor = driftFactorSticky;
        if (RightVelocity().magnitude > maxStickyVelocity)
        {
            driftFactor = driftFactorSlippy;
        }

        rb.velocity = ForwardVelocity() + RightVelocity() * driftFactor;

        if (accel)
        {
            rb.AddForce(transform.up * power);
        }
        if (brake)
        {
            rb.AddForce(transform.up * -power);
        }

        float tf = Mathf.Lerp(0, turnRadius, rb.velocity.magnitude / 2);



        rb.angularVelocity = steer * tf;


    }
    Vector2 ForwardVelocity()
    {
        return transform.up * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.up);

    }
    Vector2 RightVelocity()
    {
        return transform.right * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.right);

    }
}
