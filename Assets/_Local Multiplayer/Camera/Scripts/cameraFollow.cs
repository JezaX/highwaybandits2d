﻿using UnityEngine;

public class cameraFollow : MonoBehaviour {
    private Transform lookAt;
    private Vector3 offSet;
    
	void Start () {
        lookAt = GameObject.FindGameObjectWithTag("Center").transform;
        offSet = new Vector3(0, 0, -10);
    }
	
	void LateUpdate ()
    {
        transform.position = lookAt.position + offSet;      
    }
}
