﻿using UnityEngine;

public class carSound : MonoBehaviour {
    private float topSpeed = 100f;
    private float currentSpeed = 0;
    private float pitch = 0;

	void LateUpdate () {
        currentSpeed = transform.GetComponent<Rigidbody2D>().velocity.magnitude * 15f;
        pitch = currentSpeed / topSpeed;

        transform.GetComponent<AudioSource>().pitch = pitch;
	}
}
