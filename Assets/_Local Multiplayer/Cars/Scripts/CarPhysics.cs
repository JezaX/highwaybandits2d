﻿using UnityEngine;

public class CarPhysics : MonoBehaviour
{
    private carFeatures carFeatureControl;
#region OutsideControls
    public float steer;
    public bool accel;
    public bool brake;
    public bool dropDown;
    #endregion
#region Car physics variables
    Rigidbody2D rb;
    float power = 10f;
    float turnRadius = 100f;

    float driftFactorSlippy;
    float driftFactorSticky;
    float maxStickyVelocity;

    public float DriftFactorSticky { get { return driftFactorSticky; } set { driftFactorSticky = value; } }
    public float DriftFactorSlippy { get { return driftFactorSlippy; } set { driftFactorSlippy = value; } }
    public float MaxStickyVelocity { get { return maxStickyVelocity; } set { maxStickyVelocity = value; } }
#endregion
    private void Start()
    {
        carFeatureControl = GetComponent<carFeatures>();
        //init values
        driftFactorSticky = 0.80f;  //  drift when is stable
        driftFactorSlippy = 1f;     //  drift when is unstable
        maxStickyVelocity = 2.5f;   //  max speed for good traction
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        GetCarPhysics();
        DropItems();
    }
#region Car Physics
    void GetCarPhysics()
    {
        float driftFactor = driftFactorSticky;
        if (RightVelocity().magnitude > maxStickyVelocity)
        {
            driftFactor = driftFactorSlippy;
        }

        rb.velocity = ForwardVelocity() + RightVelocity() * driftFactor;

        if (accel)
        {
            rb.AddForce(transform.up * power);
        }
        if (brake)
        {
            rb.AddForce(transform.up * -power);
        }

        float tf = Mathf.Lerp(0, -turnRadius, rb.velocity.magnitude / 2);

        rb.angularVelocity = steer * tf;
    }
    Vector2 ForwardVelocity()
    {
        return transform.up * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.up);

    }
    Vector2 RightVelocity()
    {
        return transform.right * Vector2.Dot(GetComponent<Rigidbody2D>().velocity, transform.right);

    }
    #endregion
    void DropItems()
    {
        if (dropDown)
        {
            carFeatureControl.dropOil = true;
        }
        else
        {
            carFeatureControl.dropOil = false;
        }
    }
}
