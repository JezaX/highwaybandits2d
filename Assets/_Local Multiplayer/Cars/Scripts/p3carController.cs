﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class p3carController : MonoBehaviour {

    private CarPhysics outsideControls;

    void Start()
    { 
        outsideControls = GetComponent<CarPhysics>();
    }


    void FixedUpdate()
    {
        //I, K, J, L
        outsideControls.steer = Input.GetAxis("P3Horizontal");
        outsideControls.accel = Input.GetButton("P3Accel");
        outsideControls.brake = Input.GetButton("P3Brakes");
        outsideControls.dropDown = Input.GetButtonDown("P1Drop");
    }
}
