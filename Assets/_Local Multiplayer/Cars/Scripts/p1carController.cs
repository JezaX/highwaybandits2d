﻿using UnityEngine;

public class p1carController : MonoBehaviour {
    private CarPhysics outsideControls;

    void Start ()
    {
        outsideControls = GetComponent<CarPhysics>();
        var carModelSettings = GameObject.Find("local_multiplayer_manager");
        var carModelSpecificSettings = carModelSettings.GetComponent<localMultiplayerSettings>(); // we need go from scene local_multiplayer to track01 otherwise we get error

        gameObject.GetComponent<SpriteRenderer>().sprite = carModelSpecificSettings.CarSprite;
	}
	
	void FixedUpdate ()
    {
        //arrows
        outsideControls.steer = Input.GetAxis("P1Horizontal");
        outsideControls.accel = Input.GetButton("P1Accel");
        outsideControls.brake = Input.GetButton("P1Brakes");
        outsideControls.dropDown = Input.GetButtonDown("P1Drop");
    }
}
