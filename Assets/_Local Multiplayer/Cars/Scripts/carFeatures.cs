﻿using UnityEngine;

public class carFeatures : MonoBehaviour
{
#region Variables
    public GameObject center;

    [SerializeField]private bool isFreeToDrive;

    public bool IsFreeToDrive { get { return isFreeToDrive; } set { isFreeToDrive = value; }}

    public bool isOil;  // did the car pick up the oil?
    public bool dropOil;    //  did the car drop the oil?
    public GameObject oil;
    public GameObject oilBarrel;
#endregion
    void Start()
    {
        isFreeToDrive = true;
        isOil = false;
    }

    void FixedUpdate()
    {
        /*if (isFreeToDrive == false)     // bug JX  //  change it
        {
            transform.position = center.transform.position;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<CarPhysics>().enabled = false;
        }*/

        if (isOil == true)
        {
            oilBarrel.GetComponent<SpriteRenderer>().enabled = true;    //  barrel appears on the car
            if (dropOil)
            {
                Instantiate(oil, transform.position - (transform.up * 1.5f), transform.rotation);   // drop oil behind the car
                oilBarrel.GetComponent<SpriteRenderer>().enabled = false;   //  barrel disappears from the car
                isOil = false;  //  no oil anymore for this car
            }
        }
    }
}
