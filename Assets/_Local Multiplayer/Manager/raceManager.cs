﻿using System.Linq;
using UnityEngine;

public class raceManager : MonoBehaviour
{
    #region GameObjects
    public Transform[] path;
    private GameObject[] cars;

    public GameObject firstCamPoint;
    public GameObject lastCamPoint;
#endregion
    #region Variables
    private float lerpSmoothing = 2.5f; //  switch camera smoothing
    private float reachDist = 4.7f;     //  distance to move the point along the path
    private float[] carDistances;       //  distance between car and currentPoint 
    private int sizeOfCarList;

    private float distanceVector;       //  distance between frontCamPoint and rearCamPoint
    private float maxDistance = 15;     //  max distance for active car

    public int currentPoint = 0;        //  current point on the path
    public int currentCar = 0;          //  current car in the array
#endregion

    private void FixedUpdate()
    {
        distanceVector = Vector3.Distance(firstCamPoint.transform.position, lastCamPoint.transform.position);
        cars = GameObject.FindGameObjectsWithTag("Player");
        sizeOfCarList = cars.Length;
        carDistances = new float[sizeOfCarList];

        GetCarDistance();
        AssignCamera();
        GetCarOut();
        CamPathFollow();
        Debug.Log(distanceVector);
    }
    void GetCarDistance()
    {
        for (int i = 0; i < carDistances.Length; i++)
        {
            carDistances[i] = Vector3.Distance(path[currentPoint].position, cars[i].transform.position);
        }
    }

    void AssignCamera()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            if (carDistances.Min() == carDistances[i])  //  assign frontCamPoint to the car with the lowest distance
            {
                firstCamPoint.transform.position = Vector3.Lerp(firstCamPoint.transform.position, cars[i].transform.position, lerpSmoothing * Time.deltaTime);
            }
        }

        for (int i = 0; i < cars.Length; i++)
        {
            if (carDistances.Max() == carDistances[i])  //  assign rearCamPoint to the car with the highest distance
            {
                lastCamPoint.transform.position = Vector3.Lerp(lastCamPoint.transform.position, cars[i].transform.position, lerpSmoothing * Time.deltaTime);
                Invoke("TagLastCar", 3);    //  we need to set tag after the rearCamPoint has been assigned
            }
            else
            {
                cars[i].GetComponent<carFeatures>().IsFreeToDrive = true;
            }

        }
    }

    void TagLastCar()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            if (carDistances.Max() == carDistances[i])  //  assign rearCamPoint to the car with the highest distance
            {
                cars[i].GetComponent<carFeatures>().IsFreeToDrive = false;
            }
        }
    }

    void GetCarOut()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            if (distanceVector > maxDistance && cars[i].GetComponent<carFeatures>().IsFreeToDrive == false)
            {
                cars[i].tag = "nonActive";
                cars[i].GetComponent<CarPhysics>().enabled = false;
                cars[i].GetComponent<carFeatures>().enabled = false;
            }
        }
    }

    void CamPathFollow()
    {
        Vector3 dir = path[currentPoint].position - transform.position;
        transform.position += dir * Time.deltaTime;
        if (carDistances.Min() <= reachDist)    //  if is car near the current point, then the point moves to the next
        {
            currentPoint++;
        }
        if (currentPoint >= path.Length)
        {
            currentPoint = 0;
        }
    }

    private void OnDrawGizmos()     // just editor stuff
    {

        if (path.Length > 0)
            for (int i = 0; i < path.Length; i++)
            {
                Vector2 currentNode = path[i].position;
                //Vector2 previousNode = path[i+1].position;
                if (path[i] != null)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawSphere(path[i].position, 0.5f);
                    //Gizmos.DrawLine(previousNode, currentNode);
                }
            }
    }
}
