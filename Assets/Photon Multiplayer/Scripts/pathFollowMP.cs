﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;

public class pathFollowMP : Photon.MonoBehaviour {
    private NetworkManager myNetwork;
    private float lerpSmoothing = 2.5f;
    public Transform[] path;
    public GameObject[] cars;
    public float speed = 1f;
    private float reachDist = 0.1f;
    private float[] carDistances;

    private float maxDistance = 40;
    public GameObject firstCamPoint;
    public GameObject lastCamPoint;

    public int currentPoint = 0;
    public int currentCar = 0;

    public int carID;

    //    public Text firstPositionText;
    private void Start()
    {

    }


    private void FixedUpdate()
    {
        var playerFind = GameObject.FindGameObjectWithTag("Player");
        cars[0] = playerFind;
        Debug.Log(playerFind.transform.position);
        PhotonPlayer.Find(1);

        Debug.Log(carID);
        for (int i = 0; i < 5; i++)
        {

        }

        carDistances = new float[3];
        carDistances[0] = Vector3.Distance(path[currentPoint].position, cars[0].transform.position);
       // carDistances[1] = Vector3.Distance(path[currentPoint].position, cars[1].transform.position);
        //carDistances[2] = Vector3.Distance(path[currentPoint].position, cars[2].transform.position);
        PathFollowMethod();
         Debug.Log(carDistances.Min());
       /* float distanceVector = Vector3.Distance(firstCamPoint.transform.position, lastCamPoint.transform.position);
        if (distanceVector >= maxDistance)
        {
            cars[1].SetActive(false);
        }*/



        for (int i = 0; i < cars.Length; i++)
        {
            if (carDistances.Min() == carDistances[i])
            {
                firstCamPoint.transform.position = Vector3.Lerp(firstCamPoint.transform.position, cars[i].transform.position, lerpSmoothing * Time.deltaTime);
            }

        }
        for (int i = 0; i < cars.Length; i++)
        {
            if (carDistances.Max() == carDistances[i])
            {
                lastCamPoint.transform.position = Vector3.Lerp(lastCamPoint.transform.position, cars[i].transform.position, lerpSmoothing * Time.deltaTime);
            }

        }
    }
    private void OnDrawGizmos()
    {

        if (path.Length > 0)
        for (int i = 0; i < path.Length; i++)
        {
                Vector2 currentNode = path[i].position;
                //Vector2 previousNode = path[i+1].position;

                if (path[i] != null)
            {
                Gizmos.DrawSphere(path[i].position, 1f);
                //Gizmos.DrawLine(previousNode, currentNode);
                }
            }

    }
    void PathFollowMethod()
    {
        Vector3 dir = path[currentPoint].position - transform.position;
        transform.position += dir * Time.deltaTime;
        if (carDistances.Min() <= reachDist)
        {
            currentPoint++;
        }
        if (currentPoint >= path.Length)
        {
            currentPoint = 0;
        }
    }



}
