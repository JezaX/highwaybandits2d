﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

public class NetworkPlayer : PunBehaviour {
    private CarPhysics outsideControls;

    private float lastSynchronizationTime = 0f;
    private float syncDelay = 1f;

    private Vector3 correctPlayerPos;
    private Quaternion correctPlayerRot;
    private Vector3 currentVelocity;

    private Rigidbody2D rb;
    float lerpSmoothing = 5f;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate () {
        if (photonView.isMine)
        {
            GetComponent<CarPhysics>().enabled = true;
            GetComponent<p1carController>().enabled = true;
        }
        if (!photonView.isMine)
        {

            SyncedMovement();
        }
	}
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext((float)outsideControls.steer);
            stream.SendNext((bool)outsideControls.accel);
            stream.SendNext((bool)outsideControls.brake);

            stream.SendNext(transform.rotation);
            stream.SendNext(transform.position);
            stream.SendNext(rb.velocity);

        }
        else
        {
            outsideControls.steer = (float)stream.ReceiveNext();
            outsideControls.accel = (bool)stream.ReceiveNext();
            outsideControls.brake = (bool)stream.ReceiveNext();

            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();
            currentVelocity = (Vector3)stream.ReceiveNext();

            lastSynchronizationTime = Time.time;

        }
    }

    void SyncedMovement()
    {
        Vector3 projectedPosition = this.correctPlayerPos + currentVelocity * (Time.time - lastSynchronizationTime);
        transform.position = Vector3.Lerp(transform.position, projectedPosition, Time.deltaTime * syncDelay);
        transform.rotation = Quaternion.Lerp(transform.rotation, this.correctPlayerRot, Time.deltaTime * syncDelay);
    }


}
