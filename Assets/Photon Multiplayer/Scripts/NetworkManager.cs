﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour {
    const string VERSION = "alpha 0.0.1";
    public string roomName = "Highway Bandits ROOM";
    public string playerPrefabName = "CarMultiplayer";
    public Transform spawnPoint;

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings(VERSION);
    }
    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }

    void OnJoinedLobby()
    {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = false, MaxPlayers = 4 };
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
    }
    void OnJoinedRoom()
    {
        PhotonNetwork.Instantiate(playerPrefabName,
            spawnPoint.transform.position,
            spawnPoint.transform.rotation,
            0);
    }
}
